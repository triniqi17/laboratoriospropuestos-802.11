#   Laboratorios Propuestos Ciberseguridad en redes 802.11

El siguiente proyecto se estructura en tres ataques

-   **Laboratorio 1 - Deautenticación**: Este laboratorio consiste en enseñar cómo
funciona la deautenticación en redes inalámbricas y cómo puede ser utilizada para
interrumpir la conexión de un dispositivo a una red sin que la víctima sospeche.
-   **Laboratorio 2 - Rogue AP**: En este laboratorio se aprenderá a cómo configurar un punto de acceso falso (Rogue AP) en base al AP al que está conectado
la víctima, y a cómo puede ser utilizado para interceptar el tráfico de red de
usuarios conectados a este.
-   **Laboratorio 3 - DNS Hijacking**: Este laboratorio se aprenderá a cómo realizar un ataque de DNS Hijacking y a
cómo recolectar información redirigiendo al usuario desde una página web segura
a una falsa en redes WPA2-PSK.