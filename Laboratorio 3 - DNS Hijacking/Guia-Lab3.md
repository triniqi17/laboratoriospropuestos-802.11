# Ejecución del Escenario

Para la ejecución del escenario, hemos seguido los siguientes pasos:

1. Primero de todo, se ejecutarán los ataques de ARP Spoofing y DNS Spoofing:

   ![Paso 1](img/1.png)


   Podemos ver también como cada vez que el usuario accede a la página web, este lo indica por terminal:

```plaintext
[sys.log] [inf] dns.spoof sending spoofed DNS reply for entrada.um.es (->10.0.0.1) to
10.0.0.2 : e4:e1:30:97:fb:b5 10.0.0.0/24 > 10.0.0.1 »
[23:10:21] [sys.log] [inf] dns.spoof sending spoofed DNS reply for entrada.um.es (->10.0.0.1)
to 10.0.0.2 : e4:e1:30:97:fb:b5 10.0.0.0/24 > 10.0.0.1 »
```

2.Seguidamente, clonaremos el sitio web. Este se puede realizar tanto como sitio
web no seguro (http) como seguro (https).
Luego, una vez lanzados, desde el terminal conectado, accederemos a la página
web. Deberíamos ver cómo el sitio web es igual a este, pero no es confiable, ya
sea porque no confia en el certificado emitido por la CA, o directamente es un
sitio web no seguro. En el dispositivo conectado al Rogue AP, si se accede a la
web en concreto, se observará lo siguiente:

   ![Paso 2](img/2.png)


También puede verse el certificado falso emitido por la CA creada (si hemos
elegido crear el servidor de Apache con un certificado

   ![Paso 2.1](img/2.1.png)


3. Una vez introducidos datos en el formulario, se debe ver cómo podemos obtener
los datos de la víctima:




   ![Paso 3](img/3.png)

De esta manera, hemos registrado un ataque de DNS Hijacking tanto en HTTP
como en HTTPS, y además en HTTP hemos conseguido un robo de credenciales
básico