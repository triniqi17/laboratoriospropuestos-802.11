# LABORATORIO 1 - DEAUTENTICACIÓN

- Pasos previos 

   1. Lanzamiento del sistema con docker
      1. `docker-compose build`
      2. `docker-compose up`
      3. [en otra terminal] `docker exec -it Lab1-Deautenticacion /bin/bash`



   2. Creación de un AP con un adaptador de red externo y hostapd

      1. Instalar en el sistema anfitrión los drivers del adaptador de red externo. En caso de ser el **TP LINK WN-722N** v2 o v3, seguir las siguientes intrucciones:
         - `git clone https://github.com/ivanovborislav/rtl8188eu`
         - En el directorio, `./install.sh`.
      2. Una vez descargados los drivers, debemos desactivar el DNS de nuestro ordenador, para que no entre en conflicto con el DNS de Docker. Esto se resuelve con los comandos `sudo systemctl disable systemd-resolved.service` y reiniciando el ordenador. Lo mismo con `enable`para iniciar el DNS de nuevo, reiniciándolo otra vez. 

      3. Una vez desconectado el DNS, se activa el Docker. De esta manera, lo único que se ha de hacer es **ejecutar el script** `./initSAP <interfaz del AP> <interfaz del PC>`. El AP comenzará a escuchar. 

      4. Con un dispositivo (móvil, tablet, u otro ordenador):
         - Conecta el dispositivo al punto de acceso
         - Observa lo que ocurre cuando ejecutas el ataque de deautenticación


