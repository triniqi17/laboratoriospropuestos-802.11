# Ejecución del Escenario

Para la ejecución del escenario, hemos seguido los siguientes pasos:

1. Iniciamos primero el interfaz de red.

   ![Paso 1](img/1.png)

2. Escuchamos la actividad inalámbrica:

   ![Paso 2](img/2.png)

En este caso, podemos ver que el AP al que tenemos como objetivo tiene como MAC **AA:DB:03:50:48:6A**, y vemos que se sitúa en el canal 4, y que además usa WPA2 como autenticación. Además, podemos ver ya una posible estación a usar como víctima para nuestro ataque.

3. Ahora vamos a situarnos en el canal en el que esté el AP objetivo para escuchar posibles estaciones conectadas:

   ![Paso 3](img/3.png)

   Podemos ver que hay un dispositivo conectado con una MAC **1A:61:94:FE:A8:FE**, que será a la cual lanzaremos paquetes de deautenticación para desconectarla y que realice la asociación de nuevo.

4. De esta forma, procedemos con el ataque de deautenticación.
   
   ![Paso 4](img/5.png)

   Podemos ver cómo se están enviando 5 grupos de 64 paquetes de deautenticación al dispositivo especificado. Veremos como efecto en nuestro dispositivo víctima que se finaliza la conexión con el AP. De esta forma, la víctima deberá volver a conectarse, y es ahí donde nos encontramos con el intercambio de claves EAPOL:

   ![Paso 5](img/4.png)
