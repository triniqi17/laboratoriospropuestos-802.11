# Ejecución del Escenario

Para la ejecución del escenario, hemos seguido los siguientes pasos:

1. En primer lugar, realizaremos el ataque de deautenticación (de la misma manera
que en el laboratorio 1), para capturar las trazas de autenticación y asociación.
2. Luego, editaremos el archivo hostapd.conf con la MAC y el BSSID del AP al
que vamos a suplantar. En este caso, al ser el mismo dispositivo, esto no cambiará.


3. Realizamos un ataque de fuerza bruta para descifrar la contraseña. Existen muchos, en este caso, se ha realizado por diccionario, donde, entre otras palabras,
hemos insertado la contraseña. 


   ![Paso 3](img/1.png)

4. Una vez averiguada la contraseña, configuraremos el Rogue AP en su completitud
editando el archivo hostapd.conf con la contraseña nueva. En este caso, al ser
el mismo dispositivo, esto no cambiará. 

5. Lanzamos el Rogue AP de nuevo, viendo como el usuario vuelve a conectarse:
   ![Paso 5](img/2.png)

De esta manera, hemos atacado a un terminal haciéndonos pasar por el AP al que está conectado.
